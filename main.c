/**
* @abstract Main source code and point of entry for Grand Prix 4 game server
* @author VizCreations
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[], char *env[]) {
	printf("Server started..\n");
	sleep(1);
	return 0;
}


